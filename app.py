from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify
from lib.header import Header
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/url/', methods=['GET'])
def get_long_url():
    if request.method == 'GET':
        header = Header(short_url=request.args.get('url'))
        header = header.get_header()

    if header is False or header.get('Location') is None:
        return jsonify(error='Not Found')

    return jsonify(long_url=header.get('Location'))

if __name__ == '__main__':
    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(8181, address='longurl.bjarneo.codes')
    IOLoop.instance().start()
