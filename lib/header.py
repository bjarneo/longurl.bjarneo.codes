import requests


class Header:
    def __init__(self, short_url):
        self.short_url = short_url

    def get_header(self):
        try:
            r = requests.get(self.short_url, allow_redirects=False)
        except requests.exceptions.RequestException as e:
            print e # should log this somewhere
            return False

        return r.headers